/// SCA2 Grammar

<sca2-expr> ::= <rule>
              | <rewrite-rule>
              | <class-def>
              | <comment>

<rule> ::= <subst-rule>
         | <epenthesis-rule>
         | <deletion-rule>
         | <metathesis-rule>
         | <reduplication-rule>

<rewrite-rule> ::= TEXT "|" TEXT EOL

<class-def> ::= CAPITAL "=" <chars-raw> EOL

<comment> ::= "-" <chars-raw> EOL

<subst-rule>           ::= <input> <i-separator> <output>   <context>
<epenthesis-rule>      ::=         <i-separator> <output>   <context>
<deletion-rule>        ::= <input> <i-separator>            <del-context>
<metathesis-rule>      ::= <input> <i-separator> "\\"       <context>
<reduplication-rule>   ::= <input> <i-separator> <r-output> <context>


<input>    ::= <char-or-cat-list>
<output>   ::= <char-or-cat-list>
<r-output> ::= <output> | <cat> "²"

<char-or-cat-list> ::= { <char-or-cat> }+
<char-or-cat>      ::= CHAR | <cat>
<cat>              ::= CAT-NAME | <cat-lit>
<cat-lit>          ::= "[" { CHAR } "]"

<del-context>       ::= SEPARATOR <decorated-ctx-els> USCORE [ "²" ] <decorated-ctx-els> [ SEPARATOR <exception> ] EOL
<context>           ::= SEPARATOR <decorated-ctx-els> USCORE         <decorated-ctx-els> [ SEPARATOR <exception> ] EOL
<exception>         ::= <ctx-els> USCORE <decorated-ctx-els>
<decorated-ctx-els> ::= { <decorated-ctx-el> }
<decorated-ctx-el>  ::= <ctx-el> | "(" <decorated-ctx-el> ")"
<ctx-els>           ::= { <ctx-el> }
<ctx-el>            ::= "#" | <char-or-ctx-cat>
<char-or-ctx-cat>   ::= CHAR | CAT-NAME | "…" | <ctx-cat-lit>
<ctx-cat-lit>       ::= "[" { <ctx-cat-lit-member> }+ "]"
<ctx-cat-lit-member>    ::= CHAR | CAT-NAME | "#"

<i-separator> ::= SEPARATOR | "→"

<chars-raw> ::= { ANY }