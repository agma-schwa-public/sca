const {app, BrowserWindow, ipcMain, Menu, MenuItem, dialog} = require('electron')
const path = require('path')
const os = require('os')

const __ipc_funs = new Map
const data_dir = app.getPath('userData')
const debug_mode = process.argv.includes('--debug-mode')
let help_window = null

class Window {
    static __next_id = 0
    static __web_contents_to_window = new Map
    static main

    constructor(file, options = {}) {
        this.id = Window.__next_id++
        this.handle = new BrowserWindow({
            width: 1000,
            height: 650,
            minWidth: 1000,
            minHeight: 650,
            frame: false,
            icon: __dirname + '/../assets/icon.png',
            webPreferences: {
                spellcheck: false,
                nodeIntegration: true,
                contextIsolation: false,
                preload: path.join(__dirname, 'preload.js')
            },
            ...options,
        })
        this.oldsize = this.handle.getSize()

        this.handle.on('resize', this.send_resized.bind(this))
        this.handle.on('maximize', () => {
            this.handle.webContents.send('maximised')
            this.send_resized()
        })
        this.handle.on('unmaximize', () => {
            this.handle.webContents.send('restored')
            this.send_resized()
        })

        Window.__web_contents_to_window.set(this.handle.webContents, this)

        this.handle.loadFile(file)
        this.handle.webContents.openDevTools({mode: 'bottom'})
        this.handle.webContents.closeDevTools()
    }

    finalise() {
        Window.__web_contents_to_window.delete(this.handle.webContents)
    }

    on(e, f) { this.handle.on(e, f) }

    send_resized() {
        let newsize = this.handle.getSize()
        this.handle.webContents.send('resized', {oldsize: this.oldsize, newsize: newsize})
        this.oldsize = newsize
    }

    static of(web_contents) { return Window.__web_contents_to_window.get(web_contents) }
}

function init() {
    ///
    /// Create the menu.
    ///
    let entries = {
        label: 'Window',
        submenu: [
            {
                label: 'Zoom In',
                accelerator: 'CmdOrCtrl+Plus',
                click: (_, __, web_contents) => void web_contents.send('zoom-in')
            },
            {
                label: 'Zoom Out',
                accelerator: 'CmdOrCtrl+-',
                click: (_, __, web_contents) => void web_contents.send('zoom-out')
            },
            {
                label: 'Reset Zoom',
                accelerator: 'CmdOrCtrl+0',
                click: (_, __, web_contents) => void web_contents.send('zoom-reset')
            },

            {
                role: 'reload',
                accelerator: 'CmdOrCtrl+R'
            },
        ]
    }

    if (debug_mode) {
        entries.submenu.push({
            role: 'toggleDevTools',
            accelerator: 'CmdOrCtrl+Shift+I',
        })
    }

    const menu = new Menu()
    menu.append(new MenuItem(entries))
    Menu.setApplicationMenu(menu)

    ///
    /// IPC communication
    ///

    /// Title bar.
    ipcMain.on('close-window', ({sender}) => {
        const w = Window.of(sender)
        w.finalise()
        w.handle.close()
    })
    ipcMain.on('minimise', ({sender}) => Window.of(sender).handle.minimize())
    ipcMain.on('maximise-restore', ({sender}) => {
        const w = Window.of(sender)
        if (w.handle.isMaximized()) {
            w.handle.restore()
            sender.send('restored')
        } else {
            w.handle.maximize()
            sender.send('maximised')
        }
        w.send_resized()
    })

    /// Miscellaneous
    ipcMain.on('log-to-console', (e, data) => { console.log(data) })
    ipcMain.on('eval', (e, data) => { eval(data) })
    ipcMain.on('__debug_mode_p', ({sender}) => { sender.send('__debug_mode_p', debug_mode) })

    /// Show the help window.
    ipcMain.on('__open_syntax_help', ({sender}) => {
        /// Focus the window if it's already open.
        if (help_window !== null) {
            help_window.handle.focus()
            return
        }

        /// Otherwise, open a new window if this was called from the main window.
        if (sender !== Window.main.handle.webContents) return
        help_window = new Window(__dirname + '/syntax.html')
        help_window.handle.on('close', () => {
            help_window.finalise()
            help_window = null
        })
    })

    /// IPC calls.
    ipcMain.on('__ipc_call', async ({sender}, {fun, params, id}) => {
        let val_or_promise = __ipc_funs.get(fun)(params)
        if ('then' in val_or_promise) val_or_promise = await val_or_promise
        sender.send('__ipc_return', {
            ret: val_or_promise,
            id: id,
        })
    })

    __ipc_funs.set('__open', async (params) => {
        if (!params.defaultPath) params.defaultPath = data_dir
        let {cancelled, filePaths} = await dialog.showOpenDialog(w, params)
        return cancelled ? null : filePaths[0]
    })

    __ipc_funs.set('__save', async (params) => {
        if (!params.defaultPath) params.defaultPath = data_dir + '/sca-session.sca.json'
        let {cancelled, filePath} = await dialog.showSaveDialog(w, params)
        return cancelled ? null : filePath
    })
}

function create_main_window() {
    /// Finally, create the main window.
    Window.main = new Window(__dirname + '/main.html')
    Window.main.on('closed', () => {
        Window.main = null
        BrowserWindow.getAllWindows().forEach(w => w.close())
        if (process.platform !== 'darwin') app.quit()
    })
}

/// Terminate the application once all windows have been closed.
app.on('window-all-closed', () => {
    if (process.platform !== 'darwin') app.quit()
})

function main() {
    init()
    create_main_window()

    /// More macOS nonsense.
    app.on('activate', () => {
        if (BrowserWindow.getAllWindows().length === 0) create_main_window()
    })
}

app.whenReady().then(main)
