if (!window.electron) throw new Error('This file may only be loaded in Electron-mode')

window.menu = {
    ///
    /// File
    ///
    saved: false,
    async new() {
        if (this.saved || (this.saved = await this.prompt_unsaved_changes())) {
            new_session()
            settings.savefile = null
        }
    },
    async open() {
        let filename = await __ipc_call('__open', {
            defaultPath: settings.savefile,
            properties: ['openFile', 'promptToCreate']
        })
        if (filename) {
            settings.savefile = filename
            this.perform_open()
        }
    },
    perform_open() {
        try {
            let data = JSON.parse(__fs.readFileSync(settings.savefile))
            for (let cm of code_mirrors) eval(`${cm}.setValue(data['${cm}'] ?? '')`)
        } catch (e) {
            console.error(e)
            Dialog.error(`Could not load session.<br><br>File '${settings.savefile}' does not appear to contain a valid session: ${e.message}`)
            settings.savefile = null
        }
    },
    perform_save() {
        let data = {}
        for (let cm of code_mirrors) eval(`data.${cm} = ${cm}.getValue()`)
        try { __fs.writeFileSync(settings.savefile, JSON.stringify(data)) } catch (e) {
            console.error(e)
            Dialog.error(`Could not save to file '${settings.savefile}': ${e.message}`)
            settings.savefile = null
        }
        return settings.savefile !== null
    },
    async prompt_unsaved_changes() {
        return await Dialog.open_confirm_dialog('Unsaved Changes', 'You have unsaved changes.<br><br>Create a new session anyway?');

    },
    async save() {
        if (!settings.savefile) return this.save_as()
        return this.perform_save()
    },
    async save_as() {
        let filename = await __ipc_call('__save', {
            defaultPath: settings.savefile,
            properties: ['createDirectory', 'showOverwriteConfirmation']
        })
        if (filename) {
            settings.savefile = filename
            return this.perform_save()
        }
        return false
    },
    async export() {
        let filename = await __ipc_call('__save', {
            properties: ['createDirectory', 'showOverwriteConfirmation']
        })
        if (!filename) return

        try { __fs.writeFileSync(filename, results_as_csv()) } catch (e) {
            console.error(e)
            Dialog.error(`Could not save to file '${settings.savefile}': ${e.message}`)
        }
    },

    ///
    /// Help
    ///
    syntax() {
        window.__ipc.send('__open_syntax_help')
    }
}