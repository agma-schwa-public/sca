#include "../../../lib/applier.hh"

#include <emscripten/bind.h>

#define IN_ENUM(value, enum) ((value) >= static_cast<decltype(value)>(enum ::$$least) && (value) < static_cast<decltype(value)>(enum ::$$count))

enum struct sca_mode : i64 {
    $$least = 0,
    normal  = $$least,
    zompist = 1,
    $$count,
};

/// Emscripten in its infinite wisdom may catch pointers to native
/// exceptions as js numbers. This function is called by the js code
/// catching the exceptions to retrieve the exception message.
///
/// See also https://github.com/emscripten-core/emscripten/issues/6330
std::string exception_message(intptr_t e) { return std::string{reinterpret_cast<std::exception*>(e)->what()}; }

std::string parse_and_apply(std::string classes, std::string rules, std::string words_raw, int mode_raw) {
    if (!IN_ENUM(mode_raw, sca_mode)) throw std::runtime_error(fmt::format("SCA Mode must be between {} and {}, but was {}", i64(sca_mode::$$least), i64(sca_mode::$$count) - 1, mode_raw).c_str());
    auto mode = static_cast<sca_mode>(mode_raw);

    /// Set up the parser and redirect diagnostics.
    std::string diagnostics;
    sca::parser p;
    p.diagnostic_handler = [&](sca::parser::diagnostic&& d) {
        diagnostics += d.message();
        diagnostics += "\f\f";
    };

    /// Make sure to use the right mode.
    switch (mode) {
        case sca_mode::normal:
            if (!p.parse_classes(sca::to_utf32(classes))) throw std::runtime_error(diagnostics.c_str());
            if (!p.parse_rules(sca::to_utf32(rules))) throw std::runtime_error(diagnostics.c_str());
            break;
        case sca_mode::zompist: {
            p.sca2_mode = true;
            auto input  = sca::to_utf32(classes);
            input += sca::to_utf32(rules);
            if (!p.zompist_parse(input)) throw std::runtime_error(diagnostics.c_str());
        } break;
        case sca_mode::$$count: UNREACHABLE();
    }

    /// Parse the words.
    auto [words, success] = p.parse_words(sca::to_utf32(words_raw));
    if (!success) throw std::runtime_error(diagnostics.c_str());

    /// Apply the words. Make sure to check for exceptions.
    std::vector<sca::word> result;
    try {
        sca::applier a{p, true};
        result = a.apply(words);
    } catch (const sca::compiler::error& e) {
        throw std::runtime_error(fmt::format("Compiler Error: {}", e.what()).c_str());
    }

    /// Join the resulting words together.
    std::string joined;
    for (const auto& item : result) {
        joined += sca::to_utf8(item.data);
        joined += "\n";
    }

    /// Return an array containing the results and diagnostics.
    joined += "\v\v";
    joined += diagnostics;
    return joined;
}

std::vector<std::array<unsigned, 4>> tokenise_text(std::string text_raw) {
    /// The argument must be a string.
    std::u32string text = sca::to_utf32(text_raw);

    /// Set up the parser and ignore diagnostics.
    sca::parser p;
    p.use_term_colours   = false;
    p.warnings           = sca::parser::warn_none;
    p.diagnostic_handler = [&](sca::parser::diagnostic&& d) {};

    /// Tokenise the text.
    p.init(text);
    auto toks = p.tokens();

    /// Add the token ranges to a vector and return it.
    std::vector<std::array<unsigned, 4>> ret;
    for (u64 i = 0; i < toks.size(); i++) {
        ret.push_back({toks[i].pos.start.line - 1,
            toks[i].pos.start.col - 1,
            toks[i].pos.end.line - 1,
            toks[i].pos.end.col - 1});
    }
    return ret;
}

EMSCRIPTEN_BINDINGS(stl_wrappers) {
    /// Enable colours for assertion errors.
    assertion_error::use_colour = true;

    emscripten::function("tokenise_text", &tokenise_text);
    emscripten::function("parse_and_apply", &parse_and_apply);
    emscripten::function("exception_message", &exception_message);
    emscripten::register_vector<std::array<unsigned, 4>>("VectorRange");
}
