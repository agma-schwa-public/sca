#include "applier.hh"

#include <iostream>
#include <random>

namespace sca {
void applier::init_from_parser(parser& p) {
    auto fsms = sca::compiler::compile(p);
    rules.resize(fsms.size());
    for (u64 i = 0; i < fsms.size(); ++i) rules[i] = {std::move(p.rules[i]), std::move(fsms[i])};
}

word applier::apply(word w) const {
    for (const auto& rule : rules) apply(rule, w);
    return w;
}

void applier::apply(const rule& r, std::vector<word>& words) const {
    PARALLEL for (u64 i = 0; i < words.size(); ++i) apply(r, words[i]);
}

auto applier::apply(std::vector<word> words) const -> std::vector<word> {
    for (auto& r : rules) apply(r, words);
    return words;
}

void applier::apply(const rule& r, word& w) const {
    auto& [rule, machine] = r;
    auto res  = machine.interpret(w);

    if (res.positions.empty()) return;
    switch (rule.type) {
        case parser::rule_type::substitution:
            for (auto it = res.positions.rbegin(); it != res.positions.rend(); ++it) {
                const parser::element& el = rule.output.els[it->which];
                w.data.replace(it->start, it->len, el.percentage == 0 ? el.text.data : el.construct_text());
            }
            break;
        case parser::rule_type::epenthesis:
            for (auto it = res.positions.rbegin(); it != res.positions.rend(); ++it) {
                const parser::element& el = rule.output.els[it->which];
                w.data.insert(it->start, el.percentage == 0 ? el.text.data : el.construct_text());
            }
            break;
        case parser::rule_type::deletion:
            for (auto it = res.positions.rbegin(); it != res.positions.rend(); ++it)
                w.data.erase(it->start, it->len);
            break;
        case parser::rule_type::metathesis:
            for (auto it = res.positions.rbegin(); it != res.positions.rend(); ++it)
                w.data.replace(it->start, it->len, rule.input.els[it->which].text.reverse());
            break;
        case parser::rule_type::reduplication:
            for (auto it = res.positions.rbegin(); it != res.positions.rend(); ++it) {
                std::u32string_view s{w.data.data() + it->start, it->len};
                for (u64 i = 0; i < rule.reps; i++) w.data.insert(it->start, s);
            }
            break;
    }
}

} // namespace sca
