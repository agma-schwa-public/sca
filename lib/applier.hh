#ifndef SCA_APPLIER_H
#define SCA_APPLIER_H

#include "compiler.hh"

namespace sca {

struct applier {
    struct rule {
        parser::rule data;
        fsm          machine;

        rule() {}
        inline rule(parser::rule&& r, fsm&& m) : data(std::move(r)), machine(std::move(m)) {}
    };
    std::vector<rule> rules;
    bool              dump_ir = false;

    explicit inline applier(parser& p, bool _dump_ir = false) : dump_ir(_dump_ir) { init_from_parser(p); }
    explicit inline applier(std::vector<rule>&& rs, bool _dump_ir = false) : rules(std::move(rs)), dump_ir(_dump_ir) {}

    void apply(const rule& r, std::vector<word>& words) const;
    void apply(const rule& r, word& w) const;

    [[nodiscard]] auto apply(std::vector<word> ws) const -> std::vector<word>;
    [[nodiscard]] word apply(word w) const;

    void init_from_parser(parser& p);
};

} // namespace sca

#endif // SCA_APPLIER_H
