#ifndef SCA_UNICODE_HH
#define SCA_UNICODE_HH

#include <unicode/uchar.h>

#define FORCEINLINE [[gnu::always_inline]] inline

namespace unicode {
static constexpr inline uint8_t cc_double_below = 233;
static constexpr inline uint8_t cc_double_above = 234;

template <uint32_t gc>
FORCEINLINE bool is_general_category(char32_t c) {
    auto cat = u_getIntPropertyValue(static_cast<UChar32>(c), UCHAR_GENERAL_CATEGORY_MASK);
    return static_cast<uint32_t>(cat) & gc;
}

FORCEINLINE bool is_mark(char32_t c) {
    return is_general_category<U_GC_M_MASK>(c);
}

FORCEINLINE bool is_control(char32_t c) {
    return is_general_category<U_GC_CC_MASK>(c);
}

FORCEINLINE bool is_whitespace(char32_t c) {
    return u_isUWhiteSpace(static_cast<UChar32>(c));
}

FORCEINLINE bool is_uppercase(char32_t c) {
    return u_isUUppercase(static_cast<UChar32>(c));
}

FORCEINLINE bool is_diacritic(char32_t c) {
    auto cat = static_cast<uint32_t>(u_getIntPropertyValue(static_cast<UChar32>(c), UCHAR_GENERAL_CATEGORY_MASK));
    return cat & U_GC_M_MASK || cat & U_GC_LM_MASK;
}

FORCEINLINE bool is_tie(char32_t c) {
    auto cat = u_getCombiningClass(static_cast<UChar32>(c));
    return cat == cc_double_above || cat == cc_double_below;
}

} // namespace unicode

#undef FORCEINLINE

#endif // SCA_UNICODE_HH
