#include "sca_c.h"
#include "applier.hh"
#include <string.h>

#define BAIL() do {\
    res->diags = strdup(diagnostics.c_str());\
    res->diags_len = diagnostics.size();\
    res->result = NULL;\
    return SCA_RESULT_ERR;\
}while(0)


extern "C" sca_result_code_t sca_parse_and_apply(
        const char* cclasses, size_t cclasses_len,
        const char* crules,   size_t crules_len,
        const char* cwords,   size_t cwords_len,
        sca_dialect_t dialect,
        sca_parse_apply_res_t* res) {
    if (!cclasses || !crules || !cwords || !res) {
        return SCA_RESULT_ERR;
    }
    std::string classes{cclasses, cclasses_len};
    std::string rules{crules, crules_len};
    std::string words_raw{cwords, cwords_len};
    std::string joined;
    std::string diagnostics;
    try {
        sca::parser p;
        p.diagnostic_handler = [&](sca::parser::diagnostic&& d) {
            diagnostics += d.message();
            diagnostics += "\n";
        };
        if (dialect == SCA_DIALECT_NORMAL) {
            if (!p.parse_classes(sca::to_utf32(classes))) BAIL();
            if (!p.parse_rules(sca::to_utf32(rules))) BAIL();
        } else if (dialect == SCA_DIALECT_ZOMPIST) {
            p.sca2_mode = true;
            auto input = sca::to_utf32(classes);
            input += sca::to_utf32(rules);
            if (!p.zompist_parse(input)) BAIL();
        } else {
            diagnostics += fmt::format("Unknown dialect: {}\n", dialect);
            BAIL();
        }

        auto [words, success] = p.parse_words(sca::to_utf32(words_raw));
        if(!success) BAIL();

        std::vector<sca::word> result;
        try {
            sca::applier a{p, true};
            result = a.apply(words);
        } catch (const sca::compiler::error& e) {
            diagnostics = fmt::format("Compiler Error: {}", e.what());
            BAIL();
        }

        for (const auto& item: result) {
            joined += sca::to_utf8(item.data);
            joined += "\n";
        }
    } catch (const std::exception& e) {
        diagnostics += fmt::format("Internal Error: {}\n", e.what());
        BAIL();
    } catch (...) {
        diagnostics += "Internal Error: Unknown error\n";
        BAIL();
    }

    res->result = strdup(joined.c_str());
    res->result_len = joined.size();
    res->diags = strdup(diagnostics.c_str());
    res->diags_len = diagnostics.size();
    return SCA_RESULT_OK;
}
