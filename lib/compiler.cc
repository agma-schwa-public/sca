#include "compiler.hh"

#include "unicode.hh"

#include <set>
#include <sstream>

#ifdef __linux__
#    include <sys/mman.h>
#endif

namespace sca {
/// Static
using el_t = parser::element_type;

///
/// Miscellaneous
///

void compiler::dump() {
    machine.dump();
}

void fsm::dump() const {
    fmt::print("===========\n"
               "=== FSM ===\n"
               "===========\n");
    std::set<u32> addresses;
    for (u64 i = 0; i < bytecode.size(); i++) {
        fmt::print("[{:08X}]: ", i);
        switch (auto opcode = bytecode[i]; opcode) {
            case fsm::i_one:
            case fsm::i_r_one: {
                i++;
                if (i > bytecode.size() - 4) {
                    fmt::print("\n !!! Decode error: missing bytes !!!\n");
                    return;
                }
                char32_t c;
                std::memcpy(&c, bytecode.data() + i, sizeof c);
                for (u64 j = i - 1; j < i + 4; j++) fmt::print("{:02X} ", bytecode[j]);
                fmt::print("              {}   {} (U+{:04X})\n",
                    opcode == fsm::i_one ? "one  " : "one_r", (unicode::is_control(c) ? "?" : to_utf8(c).c_str()), u32(c));
                i += 3;
            } break;
            case fsm::i_any:
            case fsm::i_r_any:
                fmt::print("{:02X}                           {}\n", fsm::i_any, opcode == fsm::i_one ? "one" : "one_r");
                break;
            case fsm::i_try: {
                i++;
                if (i > bytecode.size() - 8) {
                    fmt::print("\n !!! Decode error: missing bytes !!!\n");
                    return;
                }
                char32_t c;
                std::memcpy(&c, bytecode.data() + i, sizeof c);
                i += 4;
                char32_t c2;
                std::memcpy(&c2, bytecode.data() + i, sizeof c2);
                addresses.insert(c2);
                for (u64 j = i - 5; j < i; j++) fmt::print("{:02X} ", bytecode[j]);
                fmt::print("              try     [{:08X}]\n", u32(c));

                fmt::print("[{:08X}]: ", i - 1);
                for (u64 j = i; j < i + 4; j++) fmt::print("{:02X} ", bytecode[j]);
                fmt::print("                 addr    [{:08X}]\n", u32(c2));
                i += 3;
            } break;
            case fsm::i_opt: {
                i++;
                if (i > bytecode.size() - 4) {
                    fmt::print("\n !!! Decode error: missing bytes !!!\n");
                    return;
                }
                char32_t c;
                std::memcpy(&c, bytecode.data() + i, sizeof c);
                for (u64 j = i - 1; j < i + 4; j++) fmt::print("{:02X} ", bytecode[j]);
                fmt::print("              opt     [{:08X}]\n", u32(c));
                i += 3;
            } break;
            case fsm::i_neg: {
                i++;
                if (i > bytecode.size() - 4) {
                    fmt::print("\n !!! Decode error: missing bytes !!!\n");
                    return;
                }
                char32_t c;
                std::memcpy(&c, bytecode.data() + i, sizeof c);
                for (u64 j = i - 1; j < i + 4; j++) fmt::print("{:02X} ", bytecode[j]);
                fmt::print("              neg     [{:08X}]\n", u32(c));
                i += 3;
            } break;
            case fsm::i_capture_start:
                i++;
                if (i > bytecode.size() - 4) {
                    fmt::print("\n !!! Decode error: missing bytes !!!\n");
                    return;
                }
                u32 c;
                std::memcpy(&c, bytecode.data() + i, sizeof c);
                fmt::print("{:02X} {:02X} {:02X} {:02X} {:02X}               start   {}\n",
                    fsm::i_capture_start, bytecode[i], bytecode[i + 1], bytecode[i + 2], bytecode[i + 3],
                    c);
                i += 3;
                break;
            case fsm::i_capture_end:
                fmt::print("{:02X}                           end\n", fsm::i_capture_end);
                break;
            case fsm::i_accept:
                fmt::print("{:02X}                           acc\n", fsm::i_accept);
                break;
            case fsm::i_reject:
                fmt::print("{:02X}                           rej\n", fsm::i_reject);
                break;
            case fsm::i_boundary:
                fmt::print("{:02X}                           bnd\n", fsm::i_boundary);
                break;
            case fsm::i_match_class:
            case fsm::i_match_class_2:
            case fsm::i_match_class_3:
            case fsm::i_match_class_4:
            case fsm::i_match_capture:
            case fsm::i_match_capture_2:
            case fsm::i_match_capture_3:
            case fsm::i_match_capture_4:
            case fsm::i_r_match_class:
            case fsm::i_r_match_class_2:
            case fsm::i_r_match_class_3:
            case fsm::i_r_match_class_4: {
                static constexpr const char* opcode_names[] = {
                    "mcls   ",
                    "mcls2  ",
                    "mcls3  ",
                    "mcls4  ",
                    "mcap   ",
                    "mcap2  ",
                    "mcap3  ",
                    "mcap4  ",
                    "mcls_r ",
                    "mcls2_r",
                    "mcls3_r",
                    "mcls4_r",
                };

                i++;
                u32 sz;
                std::memcpy(&sz, bytecode.data() + i, sizeof sz);
                fmt::print("{:02X} {:02X} {:02X} {:02X} {:02X}               {}   {}\n",
                    opcode, bytecode[i], bytecode[i + 1], bytecode[i + 2], bytecode[i + 3],
                    opcode_names[opcode - fsm::i_match_class], sz);

                i += 4;
                for (u8 j = 0; j < sz * (((opcode - fsm::i_match_class) % 4) + 1); j++, i += 4) {
                    u32 c;
                    std::memcpy(&c, bytecode.data() + i, sizeof c);
                    fmt::print("[{:08X}]: ", (unsigned) i);
                    for (u64 k = i; k < i + 4; k++) fmt::print("{:02X} ", bytecode[k]);
                    fmt::print("                 char    {} (U+{:04X})\n", unicode::is_control(c) ? "?" : to_utf8(c).c_str(), u32(c));
                }
                i--;
            } break;
            default:
                if (i > bytecode.size() - 4) fmt::print("Invalid instruction: {:02X}\n", u32(bytecode[i]));
                else {
                    u32 c;
                    std::memcpy(&c, bytecode.data() + i, sizeof c);
                    if (!addresses.contains(char32_t(i))) fmt::print("Invalid instruction: {:02X}\n", u32(bytecode[i]));
                    else {
                        addresses.insert(c);
                        for (u64 j = i; j < i + 4; j++) fmt::print("{:02X} ", bytecode[j]);
                        fmt::print("                 addr    [{:08X}]\n", u32(c));
                        i += 3;
                    }
                }
        }
    }
}

match_context::match_context(const fsm& _machine, const word& _w)
    : machine(_machine), w(_w) {
    curr = w.data.data();
    end  = w.data.data() + w.data.size();
    ip   = machine.bytecode.data();
}

void match_context::reset() {
    ip   = machine.bytecode.data();
    curr = start + 1;
}

auto fsm::interpret(const word& w) const -> match_result {
    match_context m{*this, w};
    match_result  res;
    for (;;) {
        if (m.end == m.curr) return res;
        m.start = m.curr;
        if (m.do_match()) {
            u64               start_pos = u64(m.cap_start - m.w.data.data());
            match_result::pos p{
                .start = start_pos,
                .len   = u64(m.cap_end - m.w.data.data()) - start_pos,
                .which = m.capture_index,
            };
            res.positions.push_back(p);
        }
        m.reset();
    }
}

///
/// Compiler Implementation
///

u64 compiler::alloc_addr() {
    auto& bc   = machine.bytecode;
    u64   addr = bc.size();
    bc.resize(bc.size() + 4);
    return addr;
}

template <typename V>
void compiler::store_addr(u64 addr, V value) {
    static_assert(sizeof(V) == sizeof(u32));
    std::memcpy(machine.bytecode.data() + addr, &value, sizeof value);
}

template <fsm::opcode o, compiler::match_direction dir, typename... args_t, bool... conds_t>
void compiler::emit(args_t&&... args) {
    auto& bc = machine.bytecode;

    if constexpr (o == fsm::i_any) {
        if constexpr (dir == match_direction::forwards) bc.push_back(fsm::i_any);
        else bc.push_back(fsm::i_r_any);
    } else if constexpr (o == fsm::i_capture_start) bc.push_back(fsm::i_capture_start);
    else if constexpr (o == fsm::i_capture_end) bc.push_back(fsm::i_capture_end);
    else if constexpr (o == fsm::i_accept) bc.push_back(fsm::i_accept);
    else if constexpr (o == fsm::i_reject) bc.push_back(fsm::i_reject);
    else if constexpr (o == fsm::i_boundary) bc.push_back(fsm::i_boundary);

    else if constexpr (o == fsm::i_neg || o == fsm::i_opt) {
        static_assert(sizeof...(args) == 1 && (std::is_same_v<std::remove_cvref_t<args_t>, parser::element> && ...));
        bc.push_back(o);
        u64 addr = alloc_addr();
        compile_el<dir>(args...);
        bc.push_back(fsm::i_accept);
        store_addr(addr, u32(bc.size()));
    }

    else if constexpr (o == fsm::i_one) {
        static_assert(sizeof...(args) == 1 && (std::is_same_v<std::remove_cvref_t<args_t>, char32_t> && ...));
        if constexpr (dir == match_direction::forwards) bc.push_back(fsm::i_one);
        else bc.push_back(fsm::i_r_one);
        store_addr(alloc_addr(), args...);
    }

    else if constexpr (o == fsm::i_try) emit_try<dir>(std::forward<args_t>(args)...);

    else if constexpr (o == fsm::i_match_class) emit_i_match_class<1, dir>(args...);
    else if constexpr (o == fsm::i_match_class_2) emit_i_match_class<2, dir>(args...);
    else if constexpr (o == fsm::i_match_class_3) emit_i_match_class<3, dir>(args...);
    else if constexpr (o == fsm::i_match_class_4) emit_i_match_class<4, dir>(args...);
    else if constexpr (o == fsm::i_match_capture) emit_i_match_capture<1, dir>(args...);
    else if constexpr (o == fsm::i_match_capture_2) emit_i_match_capture<2, dir>(args...);
    else if constexpr (o == fsm::i_match_capture_3) emit_i_match_capture<3, dir>(args...);
    else if constexpr (o == fsm::i_match_capture_4) emit_i_match_capture<4, dir>(args...);

    else CONSTEXPR_NOT_IMPLEMENTED("");
}

template <compiler::match_direction dir>
void compiler::emit_try(const parser::element::elements& els, bool capture) {
    auto& bc = machine.bytecode;
    bc.push_back(fsm::i_try);
    u64 end_addr = alloc_addr();

    u64 last_addr{};
    for (u64 i = 0; i < els.size(); i++) {
        last_addr = alloc_addr();

        /// Emit the capture index.
        if (capture) {
            emit<fsm::i_capture_start, dir>();
            store_addr(alloc_addr(), u32(i));
        }

        compile_el<dir>(els[i]);
        emit<fsm::i_accept, dir>();
        store_addr(last_addr, u32(bc.size()));
    }

    store_addr(last_addr, u32(FSM_TRY_REJECT));
    store_addr(end_addr, u32(bc.size()));

    if (capture) emit<fsm::i_capture_end, dir>();
}

/// Check whether all elements in `el` are text elements containing `num` codepoints.
static bool is_class_of_size(const parser::element& el, u64 num) {
    return std::all_of(el.els.begin(), el.els.end(), [num](const parser::element& el) {
        return el.type == parser::element_type::text && el.text.data.size() == num;
    });
}

template <u64 elems_length, compiler::match_direction dir>
void compiler::emit_i_match_class(const parser::element& el) {
    static_assert(elems_length >= 1 && elems_length <= 4);
    static constexpr char32_t instrs[4]   = {fsm::i_match_class, fsm::i_match_class_2, fsm::i_match_class_3, fsm::i_match_class_4};
    static constexpr char32_t r_instrs[4] = {fsm::i_r_match_class, fsm::i_r_match_class_2, fsm::i_r_match_class_3, fsm::i_r_match_class_4};

    auto& bc = machine.bytecode;

    /// Make sure we have elements to emit.
    if (el.els.empty()) return;

    /// Maybe we can optimise this class by removing a common prefix or suffix.
    if constexpr (elems_length > 1) {
        parser::element new_el;
        char32_t        c;

        /// Check to see if we can remove the first character.
        c = el.els[0].text.data[0];
        if (std::all_of(el.els.begin(), el.els.end(), [c](const parser::element& el) { return el.text.data[0] == c; })) {
            /// Remove the first character from all elements.
            for (const auto& item : el.els) {
                word w;
                w += std::u32string_view{item.text.data.data() + 1, item.text.size() - 1};
                auto w_el = parser::element::from_text(std::move(w));
                if (!contains(new_el.els, w_el)) new_el.els.push_back(std::move(w_el));
            }

            /// Emit the character removed.
            emit<fsm::i_one, dir>(c);

            /// Emit the remaining elements.
            emit_i_match_class<elems_length - 1, dir>(new_el);
            return;
        }

        /// Check to see if we can remove the last character.
        c = el.els[0].text.data.back();
        if (std::all_of(el.els.begin(), el.els.end(), [c](const parser::element& el) { return el.text.data.back() == c; })) {
            /// Remove the last character from all elements.
            for (const auto& item : el.els) {
                word w;
                w += std::u32string_view{item.text.data.data(), item.text.size() - 1};
                auto w_el = parser::element::from_text(std::move(w));
                if (!contains(new_el.els, w_el)) new_el.els.push_back(std::move(w_el));
            }

            /// Emit the remaining elements.
            emit_i_match_class<elems_length - 1, dir>(new_el);

            /// Emit the character removed.
            emit<fsm::i_one, dir>(c);
            return;
        }
    }

    /// Append the match_instruction.
    if constexpr (dir == match_direction::forwards) bc.push_back(instrs[elems_length - 1]);
    else bc.push_back(r_instrs[elems_length - 1]);

    /// Allocate enough memory for the size of the class as well as all elements.
    u64 offs = bc.size(); /// Current offset
    bc.resize(offs + sizeof(u32) /** class size **/ + sizeof(u32) * elems_length * el.els.size() /** elems size in bytes **/);

    /// Store the size of the class.
    store_addr(offs, u32(el.els.size()));
    offs += 4;

    /// Store the elements
    for (const auto& item : el.els) {
        for (u64 i = 0; i < elems_length; i++) {
            store_addr(offs, u32(item.text.data[i]));
            offs += 4;
        }
    }
}

template <u64 elems_length, compiler::match_direction dir>
void compiler::emit_i_match_capture(const parser::element& el) {
    static_assert(elems_length >= 1 && elems_length <= 4);
    static_assert(dir == match_direction::forwards, "Cannot capture while going backwards");
    static constexpr char32_t instrs[4] = {fsm::i_match_capture, fsm::i_match_capture_2, fsm::i_match_capture_3, fsm::i_match_capture_4};

    auto& bc = machine.bytecode;

    /// Append the match_instruction.
    bc.push_back(instrs[elems_length - 1]);

    /// Allocate enough memory for the size of the class as well as all elements.
    u64 offs = bc.size(); /// Current offset
    bc.resize(offs + sizeof(u32) /** class size **/ + sizeof(u32) * elems_length * el.els.size() /** elems size in bytes **/);

    /// Store the size of the class.
    store_addr(offs, u32(el.els.size()));
    offs += 4;

    /// Store the elements
    for (const auto& item : el.els) {
        for (u64 i = 0; i < elems_length; i++) {
            store_addr(offs, u32(item.text.data[i]));
            offs += 4;
        }
    }
}

template <compiler::match_direction dir>
void compiler::compile_el(const parser::element& el) {
    switch (el.type) {
        case el_t::empty: return;
        case el_t::text:
            for (const auto& item : el.text.data) {
                emit<fsm::i_one, dir>(item);
            }
            return;
        case el_t::negative: {
            if (el.els.empty()) throw compiler::error("Cannot compile empty negative");
            emit<fsm::i_neg, dir>(el.els[0]);
            return;
        }
        case el_t::chain:
            if (el.els.empty()) throw compiler::error("Cannot compile empty chain");
            for (const auto& item : el.els) compile_el<dir>(item);
            return;
        case el_t::cclass: {
            if (el.els.empty()) throw compiler::error("Cannot compile empty class");
            if (el.els.size() > UINT32_MAX) throw compiler::error(fmt::format("Too many elements in class. Maximum is {}.", UINT32_MAX));

            /// Optimise groups whose elements all have the same length.
            /// Otherwise, use a try.
            if (is_class_of_size(el, 1)) emit<fsm::i_match_class, dir>(el);
            else if (is_class_of_size(el, 2)) emit<fsm::i_match_class_2, dir>(el);
            else if (is_class_of_size(el, 3)) emit<fsm::i_match_class_3, dir>(el);
            else if (is_class_of_size(el, 4)) emit<fsm::i_match_class_4, dir>(el);
            else emit<fsm::i_try, dir>(el.els);
            return;
        }
        case el_t::optional: {
            if (el.els.empty()) throw compiler::error("Cannot compile empty optional");
            emit<fsm::i_opt, dir>(el.els[0]);
            return;
        }
        case el_t::wildcard:
            emit<fsm::i_any, dir>();
            return;
        case el_t::list: throw compiler::error("To compile a list, use compiler::compile_capture_group instead");
        case el_t::word_boundary:
            emit<fsm::i_boundary, dir>();
            return;
        case el_t::syllable_boundary:
            throw compiler::error("Sorry, unimplemented. Cannot compile syllable boundaries.");
            return;
    }
    throw std::runtime_error{fmt::format("{} called with illegal element type '{}'", __PRETTY_FUNCTION__, enum_name(el.type))};
}

template <compiler::match_direction dir>
void compiler::compile_capture_group(const parser::element& el) {
    if (el.type != parser::element_type::list
        && el.type != parser::element_type::cclass)
        throw compiler::error(fmt::format("Capture group must be a list or class, but was {}", enum_name(el.type)));
    if (el.els.empty()) throw compiler::error("Cannot compile empty group");

    /// If we only have one input element, then we don't need a try.
    /// Optimise groups whose elements all have the same length.
    /// Otherwise, use a try.
    if (el.els.size() == 1) {
        emit<fsm::i_capture_start, dir>();
        store_addr(alloc_addr(), u32(0));
        compile_el<dir>(el.els[0]);
        emit<fsm::i_capture_end, dir>();
    } else if (is_class_of_size(el, 1)) emit<fsm::i_match_capture, dir>(el);
    else if (is_class_of_size(el, 2)) emit<fsm::i_match_capture_2, dir>(el);
    else if (is_class_of_size(el, 3)) emit<fsm::i_match_capture_3, dir>(el);
    else if (is_class_of_size(el, 4)) emit<fsm::i_match_capture_4, dir>(el);
    else emit<fsm::i_try, dir>(el.els, true);
}

auto compiler::compile(const parser& p) -> std::vector<fsm> {
    std::vector<fsm> fsms;
    fsms.reserve(p.rules.size());
    for (const auto& item : p.rules) {
        compiler c;

        /// The element before the context needs to be compiled
        /// in reverse and matched backwards.
        c.compile_el<match_direction::backwards>(item.ctx.pre);

        if (item.type == parser::rule_type::epenthesis) {
            c.emit<fsm::i_capture_start, match_direction::forwards>();
            c.store_addr(c.alloc_addr(), u32(0));
        } else c.compile_capture_group<match_direction::forwards>(item.input);
        c.compile_el<match_direction::forwards>(item.ctx.post);
        c.emit<fsm::i_accept, match_direction::forwards>();
        fsms.push_back(std::move(c.machine));
    }
    return fsms;
}

#define ip32(offs) ({char32_t _c; std::memcpy(&_c, ip + offs * sizeof _c, sizeof _c); _c; })
#define READ_ADDR() \
    ({auto _char = ip32(0); \
    ip += sizeof(char32_t); _char; })

#define SAVE()       auto _curr = curr
#define RESTORE()    curr = _curr
#define SET_IP(addr) ip = machine.bytecode.data() + (addr)

#define MATCH_CLASS_IMPL(match_forwards, class_sz, cond, ...)                                 \
    do {                                                                                      \
        /** Read the size of this class. **/                                                  \
        const u32 sz = ip32(0);                                                               \
        ip += sizeof(u32);                                                                    \
                                                                                              \
        /** Compute the end address. **/                                                      \
        const u8* end_addr = ip + sz * sizeof(char32_t) * class_sz;                           \
                                                                                              \
        /** Make sure we have at least class_sz characters. **/                               \
        if constexpr (match_forwards) {                                                       \
            if (u64(end - curr) < class_sz) return false;                                     \
        } else {                                                                              \
            if (u64(curr - w.data.data()) < class_sz) return false;                           \
        }                                                                                     \
                                                                                              \
        /** Index of the element we're matching. **/                                          \
        __VA_OPT__(u32 cap_idx = 0;)                                                          \
                                                                                              \
        while (ip < end_addr) {                                                               \
            if (cond) {                                                                       \
                /** If we're matching a capture, record its location. **/                     \
                __VA_OPT__(                                                                   \
                    static_assert(match_forwards, "Cannot capture while matching backwards"); \
                    capture_index = cap_idx;                                                  \
                    cap_start     = curr;                                                     \
                    cap_end       = curr + class_sz; /**/                                     \
                )                                                                             \
                                                                                              \
                /** Go to the next instruction. **/                                           \
                ip = end_addr;                                                                \
                if constexpr (match_forwards) curr += class_sz;                               \
                else curr -= class_sz;                                                        \
                goto next;                                                                    \
            }                                                                                 \
            ip += sizeof(char32_t) * class_sz;                                                \
            __VA_OPT__(cap_idx++;)                                                            \
        }                                                                                     \
        return false;                                                                         \
    } while (0)

auto match_context::do_match() RELEASE_NOEXCEPT -> bool {
    for (;;) {
    next:
        switch (*ip++) {
            case fsm::i_one: {
                if (curr == end || *curr != ip32(0)) return false;
                curr++;
                ip += 4;
                continue;
            }

            case fsm::i_r_one: {
                if (curr == w.data.data() || curr[-1] != ip32(0)) return false;
                curr--;
                ip += 4;
                continue;
            }

            case fsm::i_any: {
                if (curr == end) return false;
                curr++;
                continue;
            }

            case fsm::i_r_any: {
                if (curr == w.data.data()) return false;
                curr--;
                continue;
            }

            case fsm::i_opt: {
                auto skip_addr = READ_ADDR();
                SAVE();
                /// Skip the machine if it rejects
                if (!do_match()) {
                    SET_IP(skip_addr);
                    RESTORE();
                }
                continue;
            }

            case fsm::i_try: {
                /// Read the end address
                auto success_addr = READ_ADDR();
                for (;;) {
                    /// Read the address that indicates the start of the next option
                    auto next_addr = READ_ADDR();
                    SAVE();
                    /// Execute the submachine and jump to the end of this try-sequence if it accepts
                    if (do_match()) {
                        SET_IP(success_addr);
                        break;
                    }
                    /// Otherwise, move on to the next option, or reject if this was the last option
                    if (next_addr == FSM_TRY_REJECT) return false;
                    RESTORE();
                    SET_IP(next_addr);
                }
                continue;
            }

            case fsm::i_match_class: MATCH_CLASS_IMPL(true, 1, curr[0] == ip32(0));
            case fsm::i_match_class_2: MATCH_CLASS_IMPL(true, 2, curr[0] == ip32(0) && curr[1] == ip32(1));
            case fsm::i_match_class_3: MATCH_CLASS_IMPL(true, 3, curr[0] == ip32(0) && curr[1] == ip32(1) && curr[2] == ip32(2));
            case fsm::i_match_class_4: MATCH_CLASS_IMPL(true, 4, curr[0] == ip32(0) && curr[1] == ip32(1) && curr[2] == ip32(2) && curr[3] == ip32(3));

            case fsm::i_match_capture: MATCH_CLASS_IMPL(true, 1, curr[0] == ip32(0), true);
            case fsm::i_match_capture_2: MATCH_CLASS_IMPL(true, 2, curr[0] == ip32(0) && curr[1] == ip32(1), true);
            case fsm::i_match_capture_3: MATCH_CLASS_IMPL(true, 3, curr[0] == ip32(0) && curr[1] == ip32(1) && curr[2] == ip32(2), true);
            case fsm::i_match_capture_4: MATCH_CLASS_IMPL(true, 4, curr[0] == ip32(0) && curr[1] == ip32(1) && curr[2] == ip32(2) && curr[3] == ip32(3), true);

            case fsm::i_r_match_class: MATCH_CLASS_IMPL(false, 1, curr[-1] == ip32(0));
            case fsm::i_r_match_class_2: MATCH_CLASS_IMPL(false, 2, curr[-1] == ip32(0) && curr[-2] == ip32(1));
            case fsm::i_r_match_class_3: MATCH_CLASS_IMPL(false, 3, curr[-1] == ip32(0) && curr[-2] == ip32(1) && curr[-3] == ip32(2));
            case fsm::i_r_match_class_4: MATCH_CLASS_IMPL(false, 4, curr[-1] == ip32(0) && curr[-2] == ip32(1) && curr[-3] == ip32(2) && curr[-4] == ip32(3));

            case fsm::i_neg: {
                auto skip_addr = READ_ADDR();
                if (!do_match()) SET_IP(skip_addr);
                else return false;
                continue;
            }

            case fsm::i_capture_start: {
                auto capture_idx = READ_ADDR();
                capture_index    = capture_idx;
                cap_start = curr = start;
                continue;
            }

            case fsm::i_capture_end: {
                cap_end = curr;
                continue;
            }

            case fsm::i_boundary: {
                if (curr == end || curr == w.data.data()) continue;
                return false;
            }

            case fsm::i_accept: {
                return true;
            }

            case fsm::i_reject: {
                return false;
            }

            default:
#ifndef NDEBUG
                machine.dump();
                throw compiler::error(fmt::format("Invalid instruction: {:X}", u32(*ip)));
#else
                return false;
#endif
        }
    }
}
#undef MATCH_CLASS_IMPL
#undef ip32
#undef SET_IP
#undef READ_ADDR
#undef RESTORE
#undef SAVE

} // namespace sca
