#ifndef SCA_CXX_UTILS_H
#define SCA_CXX_UTILS_H

#include "../3rdparty/fmt/include/fmt/format.h"

#include <cmath>
#include <codecvt>
#include <cstdarg>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <limits>
#include <locale>
#include <string>
#include <algorithm>

#define CAT_(X, Y) X##Y
#define CAT(X, Y)  CAT_(X, Y)

#define STR_(X) #X
#define STR(X)  STR_(X)

#define CAR(X, ...) X
#define CDR(X, ...) __VA_ARGS__

#ifdef NDEBUG
#    define RELEASE_NOEXCEPT noexcept
#else
#    define RELEASE_NOEXCEPT
#endif

#define FILENAME (this_file_name())

#define TODO(...) todo_impl(FILENAME, __LINE__, __PRETTY_FUNCTION__ __VA_OPT__(, ) __VA_ARGS__)

#define GCC_DIAGNOSTIC_PUSH(D) _Pragma("GCC diagnostic push") _Pragma(STR(GCC diagnostic ignored D))
#define GCC_DIAGNOSTIC_POP(D)  _Pragma("GCC diagnostic pop")

#define ASSERT(condition, ...)                                   \
    do {                                                         \
        if (!(condition))                                        \
            throw assertion_error(STR(condition),                \
                FILENAME,                                        \
                __LINE__,                                        \
                __PRETTY_FUNCTION__ __VA_OPT__(, ) __VA_ARGS__); \
    } while (0)

#define UNREACHABLE() ASSERT(false, "UNREACHABLE")

#define CONSTEXPR_NOT_IMPLEMENTED(msg)            \
    []<bool x = false> { static_assert(x, msg); } \
    ()

#define defer   auto CAT($$defer_struct_instance_, __COUNTER__) = defer_type_operator_lhs::instance % [&]
#define tempset auto CAT($$tempset_type_instance_, __COUNTER__) = tempset_operator_lhs::instance % &

#define nocopy(type)            \
    type(const type&) = delete; \
    type& operator=(const type&) = delete

#define nomove(type)        \
    type(type&&)  = delete; \
    type& operator=(type&&) = delete

#ifdef __linux__
#    define PARALLEL _Pragma("omp parallel for")
#else
#    define PARALLEL
#endif

#ifdef EMSCRIPTEN
#    define U64_MAX UINT32_MAX
#else
#    define U64_MAX  UINT64_MAX
#endif

consteval const char* this_file_name(const char* fname = __builtin_FILE()) {
    const char *ptr = __builtin_strchr(fname, '/'), *last = ptr;
    if (!last) return fname;
    while (last) {
        ptr  = last;
        last = __builtin_strchr(last + 1, '/');
    }
    return ptr + 1;
}

template <typename callable_t>
struct defer_type {
    using callable_type = callable_t;
    const callable_type function;
    explicit defer_type(callable_t _function) : function(_function) {}
    inline ~defer_type() { function(); }
    defer_type(const defer_type&) = delete;
    defer_type(defer_type&&)      = delete;
    defer_type& operator=(const defer_type&) = delete;
    defer_type& operator=(defer_type&&) = delete;
};

struct defer_type_operator_lhs {
    static defer_type_operator_lhs instance;
    template <typename callable_t>
    auto operator%(callable_t rhs) -> defer_type<callable_t> { return defer_type<callable_t>(rhs); }
};

/// Helper struct to temporarily set a value
/// and then revert it at end of scope.
template <typename T>
struct tempset_type {
    T  oldval;
    T* ref = nullptr;

    template <typename U>
    [[nodiscard]] explicit tempset_type(T* r, U newval) : ref(r) {
        oldval = *ref;
        *ref   = std::move(newval);
    }

    explicit tempset_type();
    ~tempset_type() {
        if (ref) *ref = std::move(oldval);
    }

    nocopy(tempset_type);
    nomove(tempset_type);
};

template <typename T>
struct tempset_operator_lhs2 {
    T* ptr;
    tempset_operator_lhs2(T* _ptr): ptr(_ptr) {}
    template <typename value>
    auto operator=(value rhs) -> tempset_type<value> { return tempset_type<value>(ptr, std::move(rhs)); }
};

struct tempset_operator_lhs {
    static tempset_operator_lhs instance;
    template <typename value>
    auto operator%(value* rhs) -> tempset_operator_lhs2<value> { return tempset_operator_lhs2<value>(rhs); }
};

using i8  = int8_t;
using i16 = int16_t;
using i32 = int32_t;
using i64 = int64_t;
using u8  = uint8_t;
using u16 = uint16_t;
using u32 = uint32_t;

using u64 = size_t; /// This is actually gonna be 32 bits when compiling to web assembly

struct assertion_error : public std::runtime_error {
    static bool use_colour;

    explicit assertion_error(std::string&& message) : std::runtime_error(std::forward<std::string>(message)) {}
    template <typename... args_t>

    explicit assertion_error(const std::string& cond_mess, const char* file, int line, const char* pretty_function,
        fmt::format_string<args_t...> fmt_str = "", args_t&&... args)
        : std::runtime_error([&] {
              std::string m;

              /// The extra \033[m may seem superfluous, but having them as extra delimiters
              /// makes translating the colour codes into html tags easier.
              if (use_colour) {
                  m        = fmt::format("\033[1;31mAssertion Error\033[m\033[33m\n"
                                                "    In internal file\033[m \033[32m{}:{}\033[m\033[33m\n"
                                                "    In function\033[m \033[32m{}\033[m\033[33m\n"
                                                "    Assertion failed:\033[m \033[34m{}",
                             file, line, pretty_function, cond_mess);
                  auto str = fmt::format(fmt_str, std::forward<args_t>(args)...);
                  if (!str.empty()) {
                      m += fmt::format("\033[m\n\033[33m    Message:\033[m \033[31m");
                      m += str;
                  }
                  m += "\033[m\n";
              } else {
                  m        = fmt::format("Assertion Error\n"
                                                "    In internal file {}:{}\n"
                                                "    In function {}\n"
                                                "    Assertion failed: {}\n",
                             file, line, pretty_function, cond_mess);
                  auto str = fmt::format(fmt_str, std::forward<args_t>(args)...);
                  if (!str.empty()) {
                      m += fmt::format("    Message: ");
                      m += str;
                      m += "\n";
                  }
              }
              return m;
          }()) {}

    template <typename... args_t>
    explicit assertion_error(const assertion_error& other, args_t&&... args)
        : std::runtime_error([&] {
              std::string m{other.what()};
              if (!m.ends_with("\n")) m += '\n';
              ((m += args), ...);
              return m;
          }()) {}
};

namespace sca {

enum struct error_severity {
    warn,
    err,
    info,
};

auto to_utf8(const auto& str)
    -> decltype(std::wstring_convert<std::codecvt_utf8<char32_t>, char32_t>{}.to_bytes(str)) //
    requires requires { std::wstring_convert<std::codecvt_utf8<char32_t>, char32_t>{}.to_bytes(str); }
{
    std::wstring_convert<std::codecvt_utf8<char32_t>, char32_t> conv;
    return conv.to_bytes(str);
}

auto to_utf32(const auto& str)
    -> decltype(std::wstring_convert<std::codecvt_utf8<char32_t>, char32_t>().from_bytes(str)) //
    requires requires { std::wstring_convert<std::codecvt_utf8<char32_t>, char32_t>{}.from_bytes(str); }
{
    std::wstring_convert<std::codecvt_utf8<char32_t>, char32_t> conv;
    return conv.from_bytes(str);
}

template <typename container, typename element_type>
inline bool contains(const container& c, const element_type& el) {
    return std::find(c.begin(), c.end(), el) != c.end();
}

template <size_t sz>
struct constexpr_string {
    char data[sz]{};
    constexpr constexpr_string(const char (&str)[sz]) { std::copy_n(str, sz, data); }
};

} // namespace sca

/// For debugging
extern bool                  $$debug_break;
[[maybe_unused]] inline void $$dbg() {
    if ($$debug_break) asm volatile("int $3");
}

template <typename Number>
Number number_width(Number number) {
    if (number == 0) return Number(1);
    if (number < 0) number = -number;
    return Number(std::log10(number)) + 1;
}

/// For some ungodly reason, \' is an invalid escape sequence in json...
template <typename tstring>
std::string json_escape(const tstring& str) {
    const char *data, *end;
#ifndef EMSCRIPTEN
    if constexpr (requires {{*str } -> std::convertible_to<char>; }) {
#else
    if constexpr (requires { *str; }) {
#endif
        data = str;
        end  = data + strlen(data);
    } else {
        if constexpr (requires { str.c_str(); }) data = str.c_str();
        else if constexpr (requires { str.data(); }) data = str.data();
        else CONSTEXPR_NOT_IMPLEMENTED("tstring must implement either c_str() or data()");
        end = data + str.size();
    }

    std::string ret;
    for (; data < end; data++) {
        switch (*data) {
            case '\n': ret += "\\n"; continue;
            case '\r': ret += "\\r"; continue;
            case '\t': ret += "\\t"; continue;
            case '\v': ret += "\\v"; continue;
            case '\f': ret += "\\f"; continue;
            case '\"': ret += "\\\""; continue;
            case '\\': ret += "\\\\"; continue;
            default: ret += *data;
        }
    }
    return ret;
}

template <typename T, typename U, typename V>
void replace_all(T& str, const U& from, const V& to) {
    u64 pos = 0;
    for (;;) {
        pos = str.find(from, pos);
        if (pos == T::npos) return;
        str.replace(pos, from.size(), to);
        pos += to.size();
    }
}

bool is_whitespace(char32_t c);

[[noreturn]] void todo_impl(const char* file, int line, const char* pretty_function);

template <typename... args_t>
void todo_impl(const char* file, int line, const char* pretty_function,
    const fmt::format_string<args_t...> f, args_t&&... args) {
    std::string err{"Internal Error: "};
    err += fmt::format(f, std::forward<args_t>(args)...);
    err += fmt::format(" is not implemented\n"
                       "    In internal file {}:{}\n"
                       "    In function {}\n",
        file, line, pretty_function);
    throw std::runtime_error{err};
}

// clang-format on

#endif // SCA_CXX_UTILS_H
