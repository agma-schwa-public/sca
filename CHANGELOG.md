## 0.9.1 (beta) (TBA)

### Frontend
 - Fixed syntax highlighting for percentages.

### Libsca++
 - Fixed a parser bug that caused percentage alternatives (`%{...}`) to always reduce
   to their first element.
 - In SCA2 mode, the parser now no longer requires class names to
   be uppercase letters.
 - Fixed a bug in the interpreter that caused it to always skip 1
   character ahead when interpreting `mrgpN` and `mclsN`, rather than
   `N` characters.



## 0.9 (beta) (16 May 2022)
 - Initial release